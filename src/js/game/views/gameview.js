(function(namespace){
  $window = $(window);
  namespace.GameView = Backbone.View.extend({
    model: namespace.Game,

    events: {
      'click #fullscreen-trigger': 'onFullScreenTriggerClick'
    },

    initialize: function(){
      _.bindAll(this);

    },

    triggerFullScreen: function() {
      var el = document.documentElement;
      var rfs = el.requestFullScreen || el.webkitRequestFullScreen || el.mozRequestFullScreen;
      rfs.call(el);
    },

    exitFullScreen: function() {
      if (document.exitFullscreen) {
        document.exitFullscreen();
      } else if (document.msExitFullscreen) {
        document.msExitFullscreen();
      } else if (document.mozCancelFullScreen) {
        document.mozCancelFullScreen();
      } else if (document.webkitExitFullscreen) {
        document.webkitExitFullscreen();
      }
    },

    render: function(){

    }

  });
})(window.gameEngine);