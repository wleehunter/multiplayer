(function(namespace){

  // The collection of Target models
	namespace.TargetList = new (Backbone.Collection.extend({

		model: namespace.Target,
    url: '',
    
    parse: function(response) {
      //return response.targets;
    }

	}));

})(window.gameEngine);
