(function(namespace){

	// Model for the entries in the list
	namespace.Pointer = Backbone.Model.extend({

		defaults: {
			id: '',
			source: '',
			link: '',
      title: ''
		}

	});
})(window.gameEngine);
