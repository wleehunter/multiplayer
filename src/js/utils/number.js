 /**
* @author   Charles T. Wall III
* @version  0.2
* @since
*/

    /**
    * Comparison of caller against high and low bounds.
    *
    * Use newValue = caller.bound(low, high);
    * If caller is lower than low bound, returns low bound.
    * If caller is higher than high bound, returns high bound.
    * If caller is in-between bounds, returns caller.
    *
    * @param	low		Number, low bound for comparison, must be less than @param high.
    * @param    high	Number, high bound for comparison, must be greater than @param low.
    * @return	        Number, value after comparison.
    */
Number.prototype.bound = function(low/*Number*/, high/*Number*/){
    if(!(isNaN(low) || isNaN(high)) && low > high){
        throw new Error("low cannot be greater than high");
    }
    if(high != undefined && this > high){ 
        return high;
    }
    if(low != undefined && this < low){
        return low;
    }
    return this.valueOf()/*Number*/;
};

 Number.MAX_INT = 9007199254740992;
 Number.MIN_INT = -9007199254740992;