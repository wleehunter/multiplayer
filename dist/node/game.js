var util = require("util");
var io = require("socket.io");
var Player = require("./Player").Player;

var socket;
var players;

function init() {
  players = [];

  socket = io.listen(8000);

  socket.configure(function() {
    socket.set("transports", ["websocket"]);
    socket.set("log level", 2);
  });

  setEventHandlers();
}

var setEventHandlers = function() {
  socket.sockets.on("connection", onSocketConnection);
};

function onSocketConnection(client) {
  console.log("New player has connected: " + client.id);
  client.on("disconnect", onClientDisconnect);
  client.on("new player", onNewPlayer);
  client.on("move player", onMovePlayer);
};

function onClientDisconnect() {
  console.log("Player has disconnected: "+this.id);
  var removePlayer = playerById(this.id);
  if (!removePlayer) {
    console.log("Player not found: "+this.id);
    return;
  };

  players.splice(players.indexOf(removePlayer), 1);
  this.broadcast.emit("remove player", {id: this.id});
};

function onNewPlayer(data) {
  var newPlayer = new Player(data.x, data.y);
  newPlayer.id = this.id;

  // let all previously existing players know about the guy who just connected
  this.broadcast.emit("new player", {id: newPlayer.id, x: newPlayer.getX(), y: newPlayer.getY()});

  // let the guy who just connected know about all of the previously existing players
  var existingPlayer;
  for (var i = 0; i < players.length; i++) {
    existingPlayer = players[i];
    this.emit("new player", {id: existingPlayer.id, x: existingPlayer.getX(), y: existingPlayer.getY()});
  };

  players.push(newPlayer);
};

function onMovePlayer(data) {

};

function playerById(id) {
  var i;
  for (i = 0; i < players.length; i++) {
    if (players[i].id == id)
      return players[i];
  };
  return false;
};

init();